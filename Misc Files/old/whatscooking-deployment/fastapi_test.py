from fastapi import FastAPI
import config, rec_sys

import nltk 
nltk.download('wordnet')

app = FastAPI()

@app.get("/")
def read_root():
    return {"Hello":"world"}

@app.get('/recipe/')
def recommend_recipe(s: str):
    print(s)
    ingredients = s 
    recipe = rec_sys.RecSys(ingredients)
    
    response = {}
    count = 0
    for index, row in recipe.iterrows():
        response[count] = {
            'recipe': str(row['recipe']),
            'score': str(row['score']),
            'ingredients': str(row['ingredients']),
            'url': str(row['url'])
        }
        count += 1
    return response