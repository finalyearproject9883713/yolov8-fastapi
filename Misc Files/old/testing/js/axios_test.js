const axios = require('axios');
const FormData = require('form-data');
const fs = require('fs');
let data = new FormData();

data.append('file', fs.createReadStream('download (19).png'));

let config = {
    method: 'post',
    maxBodyLength: Infinity,
    url: 'http://127.0.0.1:8000/img_object_detection_to_json',
    headers: {
        ...data.getHeaders()
    },
    data: data
};

axios.request(config)
    .then((response) => {
        console.log(JSON.stringify(response.data));
    })
    .catch((error) => {
        console.log(error);
    });
