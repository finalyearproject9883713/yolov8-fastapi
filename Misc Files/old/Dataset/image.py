import requests
from bs4 import BeautifulSoup
import urllib.parse

def get_image_urls_from_webpage(url, class_name):
    image_urls = []
    response = requests.get(url)
    if response.status_code == 200:
        soup = BeautifulSoup(response.text, 'html.parser')
        img_tags = soup.find_all('img', class_=class_name)
        for img_tag in img_tags:
            img_url = img_tag.get('src')
            if img_url:
                img_url = urllib.parse.urljoin(url, img_url)
                image_urls.append(img_url)
    else:
        print("Failed to fetch webpage")
    return image_urls

# Example usage
url = "https://www.archanaskitchen.com/spicy-tomato-rice-recipe-in-hindi"
class_name = "img-thumbnail"
image_urls = get_image_urls_from_webpage(url, class_name)
print("Image URLs found:", image_urls)
