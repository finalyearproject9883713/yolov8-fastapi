import pandas as pd
from sklearn.feature_extraction.text import TfidfVectorizer
import pickle
import config

# load in parsed recipe dataset
df_recipes = pd.read_csv(config.PARSED_PATH)

# Tfidf needs unicode or string types
df_recipes['ingredients_parsed'] = df_recipes.ingredients_parsed.values.astype('U')

# TF-IDF feature extractor
tfidf = TfidfVectorizer()
tfidf.fit(df_recipes['ingredients_parsed'])
tfidf_recipe = tfidf.transform(df_recipes['ingredients_parsed'])

# This code is used to generate a TF-IDF (Term Frequency-Inverse Document Frequency) representation of the ingredients in a recipe dataset.

# First, the parsed recipe dataset is loaded from a CSV file using Pandas.

# Next, the 'ingredients_parsed' column is converted to Unicode strings, as required by the TfidfVectorizer.

# A TfidfVectorizer object is created, and its fit() method is called on the 'ingredients_parsed' column to learn the vocabulary and IDF (Inverse Document Frequency) from the data.

# The transform() method is then called on the same column to generate the TF-IDF representation of each recipe's ingredients.

# Finally, the trained TfidfVectorizer object and the TF-IDF representation of the recipes are saved to disk using Python's pickle module for later use.